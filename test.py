from googleapiclient.discovery import build


api_key = 'AIzaSyBhXcoHO2uqvgKAU8T0oLHVKTe1ZEnYtF8'


youtube = build('youtube', 'v3', developerKey=api_key)

query = str(input('Поиск: '))


search_response = youtube.search().list(
    q=query,
    type='video',
    part='id,snippet',
    maxResults=5
).execute()


for search_result in search_response.get('items', []):
    if search_result['id']['kind'] == 'youtube#video':
        print(f"Video title: {search_result['snippet']['title']}")
        print(f"Video URL: https://www.youtube.com/watch?v={search_result['id']['videoId']}\n")

